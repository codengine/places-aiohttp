import aiohttp
from aiohttp import web
import asyncio
import json
from abc import ABC, abstractmethod

PLACES_PAGE_SIZE = 100

GOOGLE_PLACES_API_KEY = "AIzaSyCI89A2OkFu65kHGSSTgWvUqfS8ajYHuVM"


class BaseParser(ABC):

    @abstractmethod
    def parse(self, content):
        pass


class JSONParser(BaseParser):

    def parse(self, content):
        try:
            content = str(content.replace(b'\n', b''))
            return json.loads(content)
        except Exception as exp:
            pass


class APIResponseParser(JSONParser):
    pass


class BaseProvider(ABC):

    def __init__(self):
        self.setup()

    @abstractmethod
    def setup(self):
        self.url = ""
        self.detail_url = ""
        self.response_parser = JSONParser()

    @abstractmethod
    def prepare_response(self, response):
        pass


class GooglePlacesProvider(BaseProvider):

    def __str__(self):
        return "Google Places"

    def setup(self):
        self.url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
        self.detail_url = "https://maps.googleapis.com/maps/api/place/details/json"
        self.response_parser = APIResponseParser()

    def prepare_places_request_url(self, keyword, location, radius, page_size=20, request=None, **kwargs):
        google_places_api_key = GOOGLE_PLACES_API_KEY
        # return base_url+"?query=123+main+street&location=42.3675294,-71.186966&key=%s" % google_places_api_key
        url = self.url + "?query=%s" % keyword
        if location and radius:
            url += "&location=%s&radius=%s" % (location, radius)
        url += "&key=%s" % google_places_api_key
        print(url)
        return url

    def prepare_details_place_url(self, place_id):
        google_places_api_key = GOOGLE_PLACES_API_KEY
        url = self.detail_url + "?placeid=%s&key=%s" % (place_id, google_places_api_key)
        return url

    def prepare_response(self, response):
        """
        ID
        Provider
        Name
        Description
        Location (lat, lng) (if applicable)
        Address (if applicable)
        URI of the place where more details are available
        """

        def get_location(result):
            try:
                return "%s, %s" % (result["geometry"]['location']['lat'], result["geometry"]['location']['lng'])
            except Exception as exp:
                pass

        formatted_response = []
        if response['status'] == 'OK':
            if response['results']:
                for result in response['results']:
                    location = get_location(result)
                    formatted_result = {
                            'Provider': self.__str__(),
                            'ProviderInstance': self,
                            'ID': result.get('place_id'),
                            'Name': result.get('name'),
                            'Address': result.get('formatted_address'),
                        }
                    if location:
                        formatted_result['Location'] = location
                    formatted_response += [
                        formatted_result
                    ]
        return formatted_response

    def prepare_detail_response(self, place_detail_response):
        response = {}
        if place_detail_response['status'] == 'OK' and type(place_detail_response['result']) is dict:
            response['url'] = place_detail_response['result'].get('website')
        return response


PLACES_PROVIDERS = [
    GooglePlacesProvider
]

api_prefix = '/api/v1'


async def fetch(session, url, provider):
    with aiohttp.Timeout(10):
        async with session.get(url) as response:
            return await response.json()


async def fetch_places(session, urls, loop, providers):
    results = await asyncio.gather(
        *[fetch(session, url, provider) for url, provider in zip(urls, providers)],
        return_exceptions=True
    )
    return results

async def search_places(request):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    places_providers = PLACES_PROVIDERS
    # import_module = lambda module_path: load_module(module_path)
    #
    # places_providers = [import_module(provider) for provider in places_providers]
    providers = [provider() for provider in places_providers]
    keyword = request.query.get('keyword')
    location = request.query.get('location')
    radius = request.query.get('radius')
    urls = [provider.prepare_places_request_url(keyword, location, radius) for provider in providers]
    with aiohttp.ClientSession(loop=loop) as session:
        places = loop.run_until_complete(
            fetch_places(session, urls, loop, providers))

    places = [provider.prepare_response(result) for result, provider in zip(places, providers)]

    places = [place_list[0] for place_list in places if place_list]

    filtered_places = list(filter(lambda p: 'ProviderInstance' in p and 'ID' in p, places))

    urls = [place['ProviderInstance'].prepare_details_place_url(place['ID']) for place in filtered_places]

    if len(filtered_places) != len(places):
        print("Warning! The output contains no provider instance and place id")

    with aiohttp.ClientSession(loop=loop) as session:
        places_detail = loop.run_until_complete(
            fetch_places(session, urls, loop, providers))

    final_response = []
    for place, place_detail in zip(filtered_places, places_detail):
        provider = place['ProviderInstance']
        del place['ProviderInstance']
        place_detail_response = provider.prepare_detail_response(place_detail) or {}
        final_response += [{**place, **place_detail_response}]

    response = {
        'status': 'OK',
        'count': len(final_response),
        'results': final_response
    }
    return web.Response(text=json.dumps(response), status=200)


app = web.Application()
app.router.add_get('%s/search-places/' % api_prefix, search_places)
web.run_app(app, host='0.0.0.0', port=8081)